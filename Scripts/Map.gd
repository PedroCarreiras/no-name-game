extends Node2D

var player = preload("res://RefactoredScenes/Entities/Characters/PlayerCharacter1.tscn")
var playerInstance = player.instance()

var coin = preload("res://RefactoredScenes/Pick-ups/Valuables/Coin.tscn")

var enemy = preload("res://Scenes/Enemy.tscn")

var shards = preload("res://RefactoredScenes/Pick-ups/Equipment/Weapons/Shards.tscn")

func _ready():
	playerInstance.position = Vector2(get_viewport().size.x/2, get_viewport().size.y/2)
	add_child(playerInstance)
	
#	get_tree().get_root().connect("size_changed", self, "_on_viewport_change")
#	$CanvasLayer/ParallaxBackground/Terrain.motion_mirroring = Vector2(get_viewport().size.x, get_viewport().size.y)
	$CanvasLayer/ParallaxBackground/Sky/ColorRect.rect_size = Vector2(get_viewport().size.x, get_viewport().size.y)
	$CanvasLayer/ParallaxBackground/Sky.motion_mirroring = Vector2(get_viewport().size.x, get_viewport().size.y)

#func _process(delta):
#	if Input.is_action_pressed("spawn_coin"):
#		var coinInstance = coin.instance()
#		coinInstance.position = get_global_mouse_position()
#		coinInstance.random = false
#		add_child(coinInstance)

func _input(event):
	if event.is_action_pressed("spawn_enemy"):
		var enemyInstance = enemy.instance()
		enemyInstance.position = get_global_mouse_position()
		add_child(enemyInstance)
		
	if event.is_action_pressed("spawn_coin"):
		var coinInstance = coin.instance()
		coinInstance.position = get_global_mouse_position()
		coinInstance.random = false
		add_child(coinInstance)
		
	if event.is_action_pressed("spawn_shards"):
		var shardsInstance = shards.instance()
		shardsInstance.position = get_global_mouse_position()
		add_child(shardsInstance)
		
	if event.is_action_pressed("ui_cancel"):
		#save()
		get_tree().quit()

#func _on_viewport_change():
#	$CanvasLayer/ParallaxBackground/Terrain.motion_mirroring = Vector2(get_viewport().size.x, get_viewport().size.y)
