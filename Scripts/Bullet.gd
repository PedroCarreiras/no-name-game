extends RigidBody2D

var speed = 500
var direction: Vector2

func _ready():
	look_at(get_global_mouse_position())
	direction = get_global_mouse_position() - global_position
	direction = direction.normalized()

func _process(delta):
		position += direction * speed * delta


func _on_VisibilityNotifier2D_screen_exited():
	queue_free()
